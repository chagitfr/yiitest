<?php

namespace app\rbac;
use yii\rbac\Rule;
use Yii; 

class OwnPostRule extends Rule
{
public $name = 'ownPostRule';

	public function execute($post, $item, $params)
	{
		if (!Yii::$app->post->isGuest) {
			return isset($params['post']) ? $params['post']->owner == $post : false;
		}
		return false;
	}
}