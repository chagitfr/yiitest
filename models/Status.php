<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $status_name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_name' => 'Status Name',
        ];
    }
	public static function getStatus()
	{
		$allStatus = self::find()->all();
		$allStatusArray = ArrayHelper::
					map($allStatus, 'id', 'status_name');
		return $allStatusArray;						
	}
	
	public static function getStatusWithAllStatus()
	{
		$allStatus = self::getStatus();
		$allStatus[null] = 'All Status';
		$allStatus = array_reverse ( $allStatus, true );
		return $allStatus;	
	}
}
