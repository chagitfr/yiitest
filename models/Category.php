<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $category_name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
        ];
    }
	public static function getCategory()
	{
		$allCategory = self::find()->all();
		$allCategoryArray = ArrayHelper::
					map($allCategory, 'id', 'category_name');
		return $allCategoryArray;			
	}
	
	public static function getCategoryWithAllCategory()
	{
		$allCategory = self::getCategory ();
		$allCategory [null] = 'All Category ';
		$allCategory  = array_reverse ( $allCategory , true );
		return $allCategory ;	
	}
}
