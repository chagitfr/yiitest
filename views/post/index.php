<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Category;
use app\models\Status;
use app\models\PostSearch;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'body',
            'author',
            'status',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
			
			  [
				'attribute' => 'categoryId',
				'label' => 'catagoryName',
				'value' => function($model){
					return $model->categoryItem->category_name;
					},
				'filter'=>Html::dropDownList('ActivitySearch[categoryId]', 
				$categoryId, $category, ['class'=>'form-control']),	
			],
            [
				'attribute' => 'statusId',
				'label' => 'statusId',
				'value' => function($model){
							return $model->statusItem->name;
					},		
			],
			
			
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
